package romano;

/**
 * Clase NroInvalidoException
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class NroInvalidoException extends Exception {
    
    public NroInvalidoException() {
        super("Numero invalido. Solo se permiten numeros entre 1 y 3999");
    }

} //Fin de clase NroInvalidoException

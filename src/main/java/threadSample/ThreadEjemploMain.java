package threadSample;

/**
 * Clase ThreadEjemploMain
 * Curso de Programacion Java
 * @author Derlis Zarate 
 */
public class ThreadEjemploMain {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        ThreadEjemploExtiende h1, h2, h3;
        ThreadEjemploRunn h4, h5, h6;
        
        h1 = new ThreadEjemploExtiende("Hilo 1");
        h2 = new ThreadEjemploExtiende("Hilo 2");
        h3 = new ThreadEjemploExtiende("Hilo 3");
        h4 = new ThreadEjemploRunn("Hilo 4");
        h5 = new ThreadEjemploRunn("Hilo 5");
        h6 = new ThreadEjemploRunn("Hilo 6");
        
        
        System.out.printf("\nMAIN: Hilo %d lanzado.", 1);
        h1.start();
        
        System.out.printf("\nMAIN: Hilo %d lanzado.", 2);
        h2.start();
        
        System.out.printf("\nMAIN: Hilo %d lanzado.", 3);
        h3.start();
        
        System.out.printf("\nMAIN: Hilo %d lanzado.", 4);
        new Thread(h4).start();
        
        System.out.printf("\nMAIN: Hilo %d lanzado.", 5);
        new Thread(h5).start();
        
        System.out.printf("\nMAIN: Hilo %d lanzado.", 6);
        new Thread(h6).start();     
        
        System.out.printf("\nMAIN: FIN DEL PROGRAMA.", 1);

    } //Fin de main

} //Fin de clase ThreadEjemploMain

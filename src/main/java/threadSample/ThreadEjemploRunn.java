package threadSample;

/**
 * Clase ThreadEjemploRunn
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ThreadEjemploRunn implements Runnable {
    
    private String nombreThread = "";

    public ThreadEjemploRunn(String str) {
        nombreThread = str;
    }
    
    public void run() {
        try {
            for (int i = 1; i <= 5; i++) {
                System.out.printf("\n%s iteracion %d.", nombreThread, i);
                Thread.sleep(i * 1000);
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        
        System.out.printf("\n%s MURIENDO!!! porque llego al fin de run()", nombreThread);
    }

} //Fin de clase ThreadEjemploRunn

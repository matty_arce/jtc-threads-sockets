package socketSample.client;

import java.net.*;
import java.io.*;

/**
 * Clase ClienteChat
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ClienteChat {

    public static void main(String args[]) throws Exception {
        MulticastSocket chat = new MulticastSocket(1234);
        InetAddress group = InetAddress.getByName("234.5.6.7");
        chat.joinGroup(group);
        
        System.out.println("Mensaje para el servidor:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String msg = br.readLine();
        
        DatagramPacket data = new DatagramPacket(msg.getBytes(), 0, msg.length(), group, 1234);
        chat.send(data);
        chat.close();
    }
      
} //Fin de clase ClienteChat

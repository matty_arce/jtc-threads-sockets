package socketSample.server;

import java.net.*;

/**
 * Clase ServidorChat
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ServidorChat {
    
    public static void main(String args[]) throws Exception {
        
        //Se creara un servidor multicast sobre el puerto 1234
        MulticastSocket server =  new MulticastSocket(1234);
        
        //getByName- retorna el IP del host.
        //La IP del grupo de chat sera 234.5.6.7
        InetAddress group = InetAddress.getByName("234.5.6.7");
        
        server.joinGroup(group);
        boolean infinito = true;
        
        /* Continuamente recibe datos y los imprime */
        while (infinito) {
            //se usa un buffer de 1024 caracteres
            byte buf[] = new byte[1024];
            DatagramPacket data = new DatagramPacket(buf, buf.length);
            
            //el servidor recibe los datos
            server.receive(data);
            
            //y los imprime en su salida estandar.
            String msg = new String(data.getData()).trim();
            System.out.println(msg);           
        }
        server.close();
    }

} //Fin de clase ServidorChat

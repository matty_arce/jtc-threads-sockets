package socketSample.server;

import java.net.*;
import java.io.*;

/**
 * Clase EchoServer
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class EchoServer {

    public static void main(String[] args) {
        ServerSocket servidor = null;
        Socket cliente;
        
        try {
            //Abre un servidor socket local en el puerto 1234
            servidor = new ServerSocket(1234);
        } catch (IOException ie) {
            System.err.println("Error al abrir socket. " + ie.getMessage());
            System.exit(1);
        }
        
        while (true) {
            try {
                //el servidor socket queda esperando indefinidamente conexiones
                //cuando algun cliente se conecte, en el objeto "cliente" tendremos
                //una referencia al cliente conectado
                cliente = servidor.accept();
                
                //cuando llegue a esta linea, es porque la anterior se ejecuto y avanzo
                //lo cual quiere decir que algun cliente se conecto a este servidor
                //obtenemos el OutputStream del cliente, para escribirle algo
                OutputStream clientOut = cliente.getOutputStream();
                PrintWriter pw = new PrintWriter(clientOut, true);
                
                //obtenemos el InputStream del cliente, para leer lo que nos dice
                InputStream clientIn = cliente.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
                
                //Leemos la primera linea del mensaje recibido
                String mensajeRecibido = br.readLine();
                
                //Enviamos la respuesta al cliente
                pw.print("Hola cliente, soy tu servidor. Recibi este mensaje tuyo: ");
                pw.println(mensajeRecibido);
                
                //cerramos conexion con el cliente luego de cerrar los flujos
                clientIn.close();
                clientOut.close();                
                cliente.close();
            } catch (IOException ie) {
                System.out.println("Error al procesar socket. " + ie.getMessage());
            }
        }
        //servidor.close();
    }

} //Fin de clase EchoServer
